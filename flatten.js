let filter = require('./filter');

let isNested = (elements) => {
    let nested = false;
    for (let index = 0; index < elements.length; index++) {
        if (Array.isArray(elements[index])) {
            nested = true;
            break;
        }
    }
    return nested;
}

let cb = (value) => value !== undefined;

let flatten = (elements, level = 1) => {
    if (!elements || !Array.isArray(elements) || typeof level !== 'number') {
        return undefined;
    }

    if (level < 1) {
        return elements;
    }

    let flattenedArray = [];


    flattenedArray = filter(flattenedArray.concat(...elements), cb);


    if (level === 1) {
        return flattenedArray;
    }
    else {
        const nested = isNested(flattenedArray);
        if (nested) {
            return flatten(flattenedArray, level - 1);
        } else {
            return flattenedArray;
        }
    }
};

module.exports = flatten;
