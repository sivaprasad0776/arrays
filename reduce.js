let reduce = (elements, cb, startingValue) =>{
    if(!elements || ! Array.isArray(elements)){
        return undefined;
    }
    if(elements.length === 0 ){
        return startingValue;
    }
    let index = 0;
    if(startingValue === undefined){
        startingValue = elements[0];
        index = 1;
    }
    let acc = startingValue;

    for(; index < elements.length ; index ++){
        acc = cb(acc, elements[index] , index, elements);
    }

    return acc;

};

module.exports = reduce;