const find = require('../find');
const arrays = require('../arrays');

function isEven(value){
    return value % 2 == 0;
}

let elements = arrays.items;
const result = find(elements, isEven);
console.log(result);