const map = require('../filter');
const arrays = require('../arrays');

function cb(element, index, values){
    return element % 2 == 0;
}

let elements = arrays.items;
const evenValues = map(elements, cb);
console.log(`List of even values in the array [${elements}] is [${evenValues}]`);