const each = require('../each');
const arrays = require('../arrays');

function cb(element, index){
    console.log(`Element at the index position ${index} in the given array is ${element}`);
}

let elements = arrays.items;
each(arrays.items, cb);