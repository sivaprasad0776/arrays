const map = require('../map');
const arrays = require('../arrays');

function cb(element, index){
    return element*2;
}

let elements = arrays.items;
const doubleValues = map(elements, cb);
console.log(`The result of doubling the each value in the array [${elements}] is [${doubleValues}]`);