const reduce = require('../reduce');
const arrays = require('../arrays');

let cb = (acc, val) => {
    return acc + val;
}


// function to test the max value using reduce

/* function cb(startingValue, element){
    let max = startingValue;
    if( element > startingValue){
        max = element
    }
    return max;
} */

let elements = arrays.items;
console.log(elements)
const sum = reduce(elements, cb, 10);
console.log(`Resultant sum : ${sum}`);