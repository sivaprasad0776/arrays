function each(elements, cb){
    if (elements == undefined || Array.isArray(elements) == false){
        return null;
    }
    else{
        for(let index = 0 ; index < elements.length; index++){
            cb(elements[index], index, elements);
        }

    }
}

module.exports = each;
