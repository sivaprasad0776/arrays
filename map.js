function map(elements, cb){
    if (elements == undefined || Array.isArray(elements) == false){
        return null;
    }
    else{
        let newArray = [];
        let newValue;
        for(let index = 0 ; index < elements.length; index++){
            newValue = cb(elements[index], index, elements);
            newArray.push(newValue);
        }
        return newArray;

    }
}

module.exports = map;
