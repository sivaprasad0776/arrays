function find(elements, cb) {
    let index;
    let result = false;
    if (elements == undefined || Array.isArray(elements) == false) {
        return null;
    }
    else {
        for (index = 0; index < elements.length; index++) {
            result = cb(elements[index]);
            if (result == true) {
                break;
            }
        }
        if (result == true) {
            return elements[index];
        }
        else {
            return undefined;
        }

    }
}

module.exports = find;
