function filter(elements, cb){
    if (elements == undefined || Array.isArray(elements) == false){
        return null;
    }
    else{
        let newArray = [];
        let match = false;
        for(let index = 0 ; index < elements.length; index++){
            match = cb(elements[index], index, elements);
            if (match == true){
                newArray.push(elements[index]);
            }
        }
        return newArray;
    }
}

module.exports = filter